# Complete the calculate_average function which accepts
# a list of numerical values and returns the average of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
# Pseudocode is available for you
from functools import reduce
def calculate_average(values):
    sum = reduce(lambda acc, item: acc + item, values,0)
    length = len(values)
    return round(sum/length,2)

print(calculate_average([4,4,]))
print(calculate_average([1,2,3,4,50,60,75,0]))
print(calculate_average([1,13,3,24,5,]))
print(calculate_average([10,2,3,43,5,12,12]))
print(calculate_average([1,25,3,4,52,12,44]))
