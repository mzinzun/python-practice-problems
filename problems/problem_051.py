# Write a function that meets these requirements.
#
# Name:       safe_divide
# Parameters: two values, a numerator and a denominator
# Returns:    if the denominator is zero, then returns math.inf.
#             otherwise, returns numerator / denominator
#
# Don't for get to import math!
import math
def safe_divide(a=0,b=1):
    return math.inf if b == 0 else round(a/b,3)

print(safe_divide(205,3))
print(safe_divide(205,0))
print(safe_divide(205))
print(safe_divide())
