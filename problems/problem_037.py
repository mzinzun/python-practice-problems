# Complete the pad_left function which takes three parameters
#   * a number
#   * the number of characters in the result
#   * a padding character
# and turns the number into a string of the desired length
# by adding the padding character to the left of it
#
# Examples:
#   * number: 10
#     length: 4
#     pad:    "*"
#     result: "**10"
#   * number: 10
#     length: 5
#     pad:    "0"
#     result: "00010"
#   * number: 1000
#     length: 3
#     pad:    "0"
#     result: "1000"
#   * number: 19
#     length: 5
#     pad:    " "
#     result: "   19"

def pad_left(number='', length=0, pad=''):
    padded_number = str(number)
    while len(padded_number) < length:
         padded_number = pad+padded_number
    return padded_number
print(pad_left(5, 3, '+'))
print(pad_left(101, 8, '*'))
print(pad_left(2023, 4, '+'))
print(pad_left())
