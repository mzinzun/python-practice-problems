# Write a class that meets these requirements.
#
# Name:       Student
#
# Required state:
#    * name, a string
#
# Behavior:
#    * add_score(score)   # Adds a score to their list of scores
#    * get_average()      # Gets the average of the student's scores
from functools import reduce
class Student:
    def __init__(self,name):
        self.name = name
        self.scores = []
    def add_score(self,score):
        self.scores.append(score)
        return f'Hello {self.name}.  {score} has been to your scores'
    def get_average(self):
        sum = reduce(lambda acc,num: acc+num,self.scores,0)
        average = sum/len(self.scores)
        return f'Hello {self.name}.  Your average score is {average}.'

student = Student('Billy')

print(student.add_score(100))
print(student.add_score(56))
print(student.add_score(89))
print(student.add_score(75))
print(student.get_average())
# Example:
#    student = Student("Malik")
#
#    print(student.get_average())    # Prints None
#    student.add_score(80)
#    print(student.get_average())    # Prints 80
#    student.add_score(90)
#    student.add_score(82)
#    print(student.get_average())    # Prints 84
#
# Do it without pseudocode, this time, from memory. Don't look
# at the last one you just wrote unless you really must.
