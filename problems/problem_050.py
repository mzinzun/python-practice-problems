# Write a function that meets these requirements.
#
# Name:       halve_the_list
# Parameters: a single list
# Returns:    two lists, each containing half of the original list
#             if the original list has an odd number of items, then
#             the extra item is in the first list
#
# Examples:
#    * input: [1, 2, 3, 4]
#      result: [1, 2], [3, 4]
#    * input: [1, 2, 3]
#      result: [1, 2], [3]

def half_the_list(values=[]):
    idx = int(len(values)/2)+len(values)%2
    first_half = values[slice(idx)]
    second_half = values[slice(idx,len(values))]
    return first_half,second_half

print(half_the_list([1,2,3,4]))
print(half_the_list([1,2,3,4,5]))
print(half_the_list())
