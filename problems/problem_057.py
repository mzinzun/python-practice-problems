# Write a function that meets these requirements.
#
# Name:       sum_fraction_sequence
# Parameters: a number
# Returns:    the sum of the fractions of the
#             form 1/2+2/3+3/4+...+number/number+1
#
# Examples:
#     * input:   1
#       returns: 1/2
#     * input:   2
#       returns: 1/2 + 2/3
#     * input:   3
#       returns: 1/2 + 2/3 + 3/4

def sum_fraction_sequence(x):
    sum=0
    for num in range(1,int(x)+1):
        sum = sum+num/num+1
        print(f"{num}/{num+1}+")if num<int(x) else print(f"{num}/{num+1}=")
    return sum

print(sum_fraction_sequence(input('Please enter a number: ')))
