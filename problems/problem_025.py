# Complete the calculate_sum function which accepts
# a list of numerical values and returns the sum of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
from functools import reduce
def calculate_sum(values=[]):
    return reduce(lambda acc,num:acc+num,values,0)
print(calculate_sum([2,4,5]))
