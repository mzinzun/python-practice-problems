# Complete the find_indexes function which accepts two
# parameters, a list and a search term. It returns a new
# list that contains the indexes of the search term in
# the search list.
#
# Remember that indexes in Python are zero-based. That
# means the first element in the list is index 0.
#
# Examples:
#   * search_list:  [1, 2, 3, 4, 5]
#     search_term:  4
#     result:       [3]
#   * search_list:  [1, 2, 3, 4, 5]
#     search_term:  6
#     result:       []
#   * search_list:  [1, 2, 1, 2, 1]
#     search_term:  1
#     result:       [0, 2, 4]
#
# Look up the enumerate function to help you with this problem.
# import numpy as np
def find_indexes(search_list, search_term):
    # this technique uses list comprehension with enumerate
    indexes  = [idx for (idx, item) in enumerate(search_list) if item == search_term]

    # this is the standard way of solving.  both techniques use enumerate
    # indexes = []
    # enum_list = dict(enumerate(search_list))
    # for key,value in enum_list.items():
    #     indexes.append(key )if value == search_term else None

    return indexes



find_indexes([1,2,3,4,2,], 2)
