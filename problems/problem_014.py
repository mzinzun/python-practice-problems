# Complete the can_make_pasta function to
# * Return true if the ingredients list contains
#   "flour", "eggs", and "oil"
# * Otherwise, return false
#
# The ingredients list will always contain three items.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def can_make_pasta(ingredients):
    ingredient_list = ["eggs","flour","oil"]
    ingredients.sort()
    return ingredients == ingredient_list

print(can_make_pasta(["oil","flour","eggs"]))
print(can_make_pasta(["sugar","flour","eggs"]))
print(can_make_pasta(["oil","tea","salt"]))
print(can_make_pasta(["eggs","flour","oil"]))
