# Complete the is_inside_bounds function which takes an x
# coordinate and a y coordinate, and then tests each to
# make sure they're between 0 and 10, inclusive.

def is_inside_bounds(x, y):
    x_is = True if int(x)>0 and int(x)<10 else False
    y_is = True if int(y)>0 and int(y)<10 else False
    return f"{x} and {y} is in bounds" if x_is and y_is else f"{x} and {y} is NOT in bounds"

print(is_inside_bounds(5, 20))
print(is_inside_bounds(1, 1))
print(is_inside_bounds(-5, 5))
print(is_inside_bounds(10, 10))
print(is_inside_bounds(9, 9))
print(is_inside_bounds(0, 0))
