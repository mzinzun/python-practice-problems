# Write a class that meets these requirements.
#
# Name:       BankAccount
#
# Required state:
#    * opening balance, the amount of money in the bank account
#
# Behavior:
#    * get_balance()      # Returns how much is in the bank account
#    * deposit(amount)    # Adds money to the current balance
#    * withdraw(amount)   # Reduces the current balance by amount
#
class BankAccount:
    def __init__(self,open_balance):
        self.open_balance = round(float(open_balance),2)
        self.current_balance = round(float(open_balance),2)

    def get_balance(self):
        return(f'\nYour current balance is: ${self.current_balance}\n')
    def deposit(self,deposit):
        starting_balance = self.current_balance
        self.current_balance = round(self.current_balance + float(deposit),2)
        return f"\nStarting Balance: ${starting_balance}\nDeposit: ${deposit}\nNew balance: ${self.current_balance}\n"
    def withdrawal(self,withdraw):
        starting_balance = self.current_balance
        self.current_balance = round(self.current_balance - float(withdraw),2)
        return f"\nStarting Balance: ${starting_balance}\nWithdrawal: ${withdraw}\nNew balance: ${self.current_balance}\n"

bank_account = BankAccount(100)
print(bank_account.get_balance())
print(bank_account.withdrawal(50))
print(bank_account.deposit(120))

# Example:
#    account = BankAccount(100)
#
#    print(account.get_balance())  # prints 100
#    account.withdraw(50)
#    print(account.get_balance())  # prints 50
#    account.deposit(120)
#    print(account.get_balance())  # prints 170
#
# Do it without pseudocode, this time, from memory. Don't look
# at the last one you just wrote unless you really must.
