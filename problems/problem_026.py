# Complete the calculate_grade function which accepts
# a list of numerical scores each between 0 and 100.
#
# Based on the average of the scores, the function
# returns
#   * An "A" for an average greater than or equal to 90
#   * A "B" for an average greater than or equal to 80
#     and less than 90
#   * A "C" for an average greater than or equal to 70
#     and less than 80
#   * A "D" for an average greater than or equal to 60
#     and less than 70
#   * An "F" for any other average
from functools import reduce
def calculate_grade(values):
    grade = (reduce(lambda acc,grade: acc+grade,values,0))
    if grade/len(values) >= 90:
        return 'A'
    elif grade/len(values) >= 80:
        return 'B'
    elif grade/len(values) >= 70:
        return 'C'
    elif grade/len(values) >= 60:
        return 'D'
    else:
         return 'F'

print(calculate_grade([95,80,100,89,100]))
print(calculate_grade([85,80,100,75,100]))
print(calculate_grade([85,80,70,75,62]))
print(calculate_grade([40,62,45,59,75]))
