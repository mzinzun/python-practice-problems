# Complete the sum_of_squares function which accepts
# a list of numerical values and returns the sum of
# each item squared
#
# If the list of values is empty, the function should
# return None
#
# Examples:
#   * [] returns None
#   * [1, 2, 3] returns 1*1+2*2+3*3=14
#   * [-1, 0, 1] returns (-1)*(-1)+0*0+1*1=2
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.
from functools import reduce
def sum_of_squares(values=[]):
    return (reduce(lambda acc,sqr:acc+sqr,list(map(lambda num: num*num,values)))) if len(values)>0 else None
    print(squared)

print(sum_of_squares([2,3,4,5]))
print(sum_of_squares([-1, 0, 1]))
print(sum_of_squares())
