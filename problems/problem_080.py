# Write a class that meets these requirements.
#
# Name:       Receipt
#
# Required state:
#    * tax rate, the percentage tax that should be applied to the total
#
# Behavior:
#    * add_item(item)   # Add a ReceiptItem to the Receipt
#    * get_subtotal()   # Returns the total of all of the receipt items
#    * get_total()      # Multiplies the subtotal by the 1 + tax rate
#
# Example:
#    item = Receipt(.1)
#    item.add_item(ReceiptItem(4, 2.50))
#    item.add_item(ReceiptItem(2, 5.00))
#
#    print(item.get_subtotal())     # Prints 20
#    print(item.get_total())        # Prints 22
from functools import reduce
class ReceiptItem:
    def __init__(self,quantity=0,price=0):
        self.quantity = quantity
        self.price = price

    def get_item(self):
        qty = float(self.quantity)
        price = float(self.price)
        return round(qty*price,2)

# item = ReceiptItem(10.4567,3)
# print('item', item)
# print(item.get_total())
class Receipt:
    def __init__(self,tax_rate):
        self.tax_rate = tax_rate
        self.items = []

    def show_items(self):
        return f'your items are: {self.items}'
    def add_item(self,item):
        self.items.append(item)
        return f'{item} was added'
    def get_subtotal(self):
        return round(reduce(lambda acc,item: acc+item,self.items,0),2)
    def get_total(self):
        return round(self.get_subtotal()** 1+self.tax_rate,2)

receipt = Receipt(.65)
print(receipt.add_item(ReceiptItem(4, 2.50).get_item()))
print(receipt.add_item(ReceiptItem(3, 5.50).get_item()))
print(receipt.add_item(ReceiptItem(1, 2.00).get_item()))
print(receipt.add_item(ReceiptItem(6, .50).get_item()))
print(receipt.show_items())
print(receipt.get_subtotal())
print(receipt.get_total())



# class Receipt
    # method initializer with tax rate
        # self.tax_rate = tax_rate
        # self.items = new empty list

    # method add_item(self, item)
        # append item to self.items list

    # method get_subtotal(self)
        # sum = 0
        # for each item in self.items
            # increase sum by item.get_total()
        # return sum

    # method get_total(self)
        # return self.get_subtotal() * (1 + self.tax_rate)
