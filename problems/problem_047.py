# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"
import re
def check_password(password=''):
    lc = re.search(r"[a-z]",password)
    uc = re.search(r"[A-Z]",password)
    num = re.search(r"[0-9]",password)
    sp = re.search(r"[!$@]",password)
    check_list = [lc,uc,num,sp]
    if 6 <= len(password) <= 12 and check_list.count(None) == 0:
        print(f"{password} is a valid Password")
    else:
        print(f"{password} is invalid")
check_password("aA2@")
check_password("aA2@michael")
check_password("michaelZ1")
check_password()
