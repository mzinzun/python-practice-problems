# Write a function that meets these requirements.
#
# Name:       username_from_email
# Parameters: a valid email address as a string
# Returns:    the username portion of the email address
#
# The username portion of an email is the substring
# of the email address that appears before the @
#
# Examples
#    * input:   "basia@yahoo.com"
#      returns: "basia"
#    * input:   "basia.farid@yahoo.com"
#      returns: "basia.farid"
#    * input:   "basia_farid+test@yahoo.com"
#      returns: "basia_farid+test"

import re
# def username_from_email(email):
email = 'mzinzun@gmail.com'
# email_re_ex = re.compile(r"^[a-z0-9]+@\w+[\.](com|edu|mil|info|net|org|museum)$")

# x=  re.search(email_re_ex,email)
# print(x.string)
def username_from_email(email = 'invalid'):
    email_re_ex = re.compile(r"^[a-z0-9]+@\w+[\.](com|edu|mil|info|net|org|gov)$")
    valid_email =  re.search(email_re_ex,email)
    if not valid_email is None:
        idx = valid_email.string.index('@')
        return valid_email.string[0:idx]
    else:
        return 'not a valid email'
print(username_from_email("mzinzun@hotmail.com"))
print(username_from_email("hotmail.com"))
print(username_from_email())
