# Complete the has_quorum function to test to see if the number
# of the people in the attendees list is greater than or equal
# to 50% of the number of people in the members list.

def has_quorum(attendees_list, members_list):
    return True if attendees_list >= members_list/2 else False

print(has_quorum(10,20))
print(has_quorum(10,30))
print(has_quorum(40,20))
print(has_quorum(10,10))
