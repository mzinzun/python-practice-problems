# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

def max_in_list(values = None):
    return None if values == None or values == [] else max(values)

print(max_in_list([]))
print(max_in_list([25,30,60,1]))
print(max_in_list([-1,20]))
print(max_in_list())
