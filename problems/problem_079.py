# Write a class that meets these requirements.
#
# Name:       ReceiptItem
#
# Required state:
#    * quantity, the amount of the item bought
#    * price, the amount each one of the things cost
#
# Behavior:
#    * get_total()          # Returns the quantity * price
#
# Example:
#    item = ReceiptItem(10, 3.45)
#
#    print(item.get_total())    # Prints 34.5
class ReceiptItem:
    def __init__(self,quantity=0,price=0):
        self.quantity = quantity
        self.price = price

    def get_total(self):
        qty = float(self.quantity)
        price = float(self.price)
        return f'{self.quantity} X {self.price} = {round(qty*price,3)}'

item = ReceiptItem(10.4567,3)
print(item.price)
print(item.get_total())
