# Modify the withdraw method of BankAccount so that the bank
# account can not have a negative balance.
#
# If a person tries to withdraw more than what is in the
# balance, then the method should raise a ValueError.

class BankAccount:
    def __init__(self, balance):
        self.balance = balance

    def get_balance(self):
        return f" balance: {self.balance}"

    def withdraw(self, amount):
        # If the amount is more than what is in
        # the balance, then raise a ValueError
        if self.balance - amount > 0:
            self.balance -= amount
            return f" balance: {self.balance}"
        else:
            raise ValueError('Sorry, you do mot have enough money for your withdraw request.')


    def deposit(self, amount):
        self.balance += amount
        return f" balance: {self.balance}"
bank_account = BankAccount(100)
print(bank_account.get_balance())
print(bank_account.withdraw(150))
print(bank_account.deposit(120))
