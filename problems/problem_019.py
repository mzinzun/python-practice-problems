# Complete the is_inside_bounds function which has the
# following parameters:
#   x: the x coordinate to check
#   y: the y coordinate to check
#   rect_x: The left of the rectangle
#   rect_y: The bottom of the rectangle
#   rect_width: The width of the rectangle
#   rect_height: The height of the rectangle
#
# The is_inside_bounds function returns true if all of
# the following are true
#   * x is greater than or equal to rect_x
#   * y is greater than or equal to rect_y
#   * x is less than or equal to rect_x + rect_width
#   * y is less than or equal to rect_y + rect_height

def is_inside_bounds(x, y, rect_x, rect_y, rect_width, rect_height):
    x_is = rect_x < x < rect_width+rect_x
    y_is = rect_y < y < rect_height+rect_y
    return f'the point {x,y} is in the box' if x_is and y_is else f'the point {x,y} is NOT in the box'

print(is_inside_bounds(5,5,2,2,25,15))
print(is_inside_bounds(0,0,2,2,25,15))
print(is_inside_bounds(13,10,2,2,25,15))
print(is_inside_bounds(-5,15,2,2,25,15))
print(is_inside_bounds(10,10,2,2,25,15))
