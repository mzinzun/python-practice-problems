# Write a function that meets these requirements.
#
# Name:       shift_letters
# Parameters: a string containing a single word
# Returns:    a new string with all letters replaced
#             by the next letter in the alphabet
#
# If the letter "Z" or "z" appear in the string, then
# they would get replaced by "A" or "a", respectively.
#
# Examples:
#     * inputs:  "import"
#       result:  "jnqpsu"
#     * inputs:  "ABBA"
#       result:  "BCCB"
#     * inputs:  "Kala"
#       result:  "Lbmb"
#     * inputs:  "zap"
#       result:  "abq"
#
# You may want to look at the built-in Python functions
# "ord" and "chr" for this problem

def shift_letters(strings):
    def convert(ltr):
        return chr(ord(ltr)+1) if ltr != 'z' and ltr != 'Z' else chr(ord(ltr)-25)

    return ''.join(list(map(convert,list(strings))))

print(shift_letters('Zimport'))
print(shift_letters('abba'))
print(shift_letters('kala'))
print(shift_letters('zap'))
print(shift_letters('Rtbbdrr'))
