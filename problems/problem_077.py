# Write a class that meets these requirements.
#
# Name:       Circle
#
# Required state:
#    * radius, a non-negative value
#
# Behavior:
#    * calculate_perimeter()  # Returns the length of the perimater of the circle
#    * calculate_area()       # Returns the area of the circle
#
import math
class Circle:
    def __init__(self,radius):
        self.radius = radius

    def calculate_perimeter(self):
        if float(self.radius) > 0:
            return f"perimeter: {round(2*math.pi*float(self.radius),2)}"
        else:
            raise ValueError('radius must be greater than 0')
    def calculate_area(self):
        if float(self.radius) > 0:
            return f"area: {round(math.pi*math.pow(float(self.radius),2),2)}"
        else:
            raise ValueError('radius must be greater than 0')


circle = Circle(5)
print(circle.calculate_perimeter())
print(circle.calculate_area())
# Example:
#    circle = Circle(10)
#
#    print(circle.calculate_perimeter())  # Prints 62.83185307179586
#    print(circle.calculate_area())       # Prints 314.1592653589793
#
# There is pseudocode for you to guide you.

# import math

# class Circle
    # method initializer with radius
        # if radius is less than 0
            # raise ValueError
        # self.radius = radius

    # method calculate_perimeter(self)
        # returns 2 * math.pi * self.radius

    # method calculate_area(self)
        # returns math.pi * (self.radius squared)
